let recipesSection = document.getElementById('recipesSection');
let recipeSection = document.getElementById('recipeSection');
let recipeBackground = document.getElementById('recipeBackground');
let recipeCard = document.getElementById('recipeCard');
let closeButton = document.getElementById('closeButton');
let recipesData;

fetch('http://localhost:3001/recipes')
.then(res => res.json())
.then(data => {
    if(data.length > 0) {
        recipesData = data;
        const recipeList = data.map(recipe => {
            return `
              <div class='recipe-card' onclick='viewDetails("${recipe.uuid}")' onmouseenter='flipBack(this)' onmouseleave='flipForward(this)'>
                  <div class='recipe-back'>
                      <h2>${recipe.title}</h2>
                      <h3>${recipe.description}</h3>
                  </div>
                  <div class='recipe-front'>
                      <img class='recipe-image' src='http://localhost:3001${recipe.images.medium}' alt='${recipe.title} image'/>
                  </div>
              </div>
            `
        }).join('');
        recipesSection.innerHTML = recipeList;
    } else {
        recipesSection.innerHTML = 'No recipe available';
    }
})

function viewDetails(recipeId) {
    recipeSection.style.display = 'block';
    if(recipesData !== undefined) {
        fetch('http://localhost:3001/specials')
        .then(res => res.json())
        .then(data => {
            const specialIngredients = data;
            const recipeData = recipesData.find( recipeData => {
                return recipeData.uuid === recipeId
            })
            const ingredients = recipeData.ingredients.map(ingredient => {
                let specialIngredient = specialIngredients.find(specialIngredient => specialIngredient.ingredientId === ingredient.uuid )
                if (specialIngredient) {
                    return `
                        <li>
                            ${ingredient.amount} ${ingredient.measurement} 
                            <strong class='tooltip'>${ingredient.name}
                                <span class='tooltiptext'>
                                  <span>${specialIngredient.title}</span> - <i>${specialIngredient.type}</i><br>
                                  <span>${specialIngredient.text}</span>
                                </span>
                            </strong>
                        </li>
                    `
                } else {
                    return `
                        <li>
                            ${ingredient.amount} ${ingredient.measurement} ${ingredient.name}
                        </li>
                    `
                }
            }).join('');
            const directions = recipeData.directions.map(direction => {
                let isOptional = '';
                if (direction.optional === true) {
                    isOptional = '(<i>Optional</i>)';
                } else {
                    isOptional = '';
                }
                return `
                    <li>
                        ${direction.instructions} ${isOptional}
                    </li>
                `
            }).join('');
            recipeBackground.style.backgroundImage = `url("http://localhost:3001${recipeData.images.full}")`;
            recipeCard.innerHTML = `
                <i id='closeButton' class='far fa-times-circle' onclick='closeRecipeSection()'></i>
                <div id='titleAndDetailsSection'>
                    <div id='detailsSection'>
                        <div>
                          <img id='recipeImageSmall' src='http://localhost:3001${recipeData.images.small}' alt='${recipeData.title} image'>
                        </div>
                        <h2>${recipeData.title}</h2>
                        <br>
                        <p class='description text-bold'>${recipeData.description}</p>
                        <br>
                        <table class='text-oxygen'>
                            <tr>
                                <td class='text-bold'>Preparation Time:</td>
                                <td>${recipeData.prepTime} minutes</td>
                            </tr>
                            <tr>
                                <td class='text-bold'>Cook Time:</td>
                                <td>${recipeData.cookTime} minutes</td>
                            </tr>
                            <tr>
                                <td class='text-bold'>Servings:</td>
                                <td>${recipeData.servings} people</td>
                            </tr>
                            <tr>
                                <td class='text-bold'>Post Date:</td>
                                <td>${recipeData.postDate}</td>
                            </tr>
                            <tr>
                                <td class='text-bold'>Edit Date:</td>
                                <td>${recipeData.editDate}</td>
                            </tr>
                        </table>
                    </div>
                    <div id='ingredientsSection'>
                        <h3>Ingredients</h3>
                        <br>
                        <ul class='text-oxygen'>
                            ${ingredients}
                        </ul>
                    </div>
                </div>
                <h3>Directions</h3>
                <br>
                <ol class='text-oxygen'>
                    ${directions}
                </ol>
            `
        })
    }
}

document.body.onload = () => {
    let cards = document.getElementsByClassName('recipe-card');
    let index = 0;
    function displayAnimation() {
        if(index < cards.length) {
            cards[index].style.animationName = 'fadeIn';
            cards[index].style.opacity = '1';
            index++; 
        } else {
            clearInterval(animationInterval);
        }
    }
    let animationInterval = setInterval(displayAnimation, 500);
}

function flipBack(container) {
    let cardFront = container.getElementsByClassName('recipe-front');
    let cardBack = container.getElementsByClassName('recipe-back');
    cardFront[0].style.transform = 'rotateY(0deg)';
    cardFront[0].style.animationName = 'flipBack';
    cardBack[0].style.transform = 'rotateY(180deg)';
    cardBack[0].style.animationName = 'flipForward';
}

function flipForward(container) {
    let cardFront = container.getElementsByClassName('recipe-front');
    let cardBack = container.getElementsByClassName('recipe-back');
    cardBack[0].style.transform = 'rotateY(0deg)';
    cardBack[0].style.animationName = 'flipBack';
    cardFront[0].style.transform = 'rotateY(180deg)';
    cardFront[0].style.animationName = 'flipForward';
}

function closeRecipeSection() {
    recipeSection.style.display = 'none';
}